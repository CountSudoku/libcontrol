#include <boost/test/unit_test.hpp>

#include "limiter/min_limiter.hpp"

namespace {

    BOOST_AUTO_TEST_CASE(test_minLimiter)
    {
        double minValue = 5.0;
        auto limiter = control::limiter::createMinLimiter(minValue);
        BOOST_TEST(limiter->limit(1e-17) == minValue);
        BOOST_TEST(limiter->limit(1.0) == minValue);
        BOOST_TEST(limiter->limit(4.999) == minValue);
        BOOST_TEST(limiter->limit(minValue) == minValue);
        BOOST_TEST(limiter->limit(minValue + 1e-10) != minValue);
        BOOST_TEST(limiter->limit(5.0001) == 5.0001);
        BOOST_TEST(limiter->limit(6) == 6.0);
        BOOST_TEST(limiter->limit(1e17) == 1e17);
    }
}
