#include <boost/test/unit_test.hpp>

#include "controller/proportional_controller.hpp"

namespace
{
    BOOST_AUTO_TEST_CASE(test_ProportionalController)
    {
        control::controller::ControllerUniquePtr controller;
        BOOST_REQUIRE_NO_THROW(
            controller = control::controller::createProportionalController(1));

        BOOST_TEST(controller->control(0) == 0);
        BOOST_TEST(controller->control(100) == 100);
        BOOST_TEST(controller->control(2) == 2);
    }

    BOOST_AUTO_TEST_CASE(test_ProportionalController_gain)
    {
        // gain = 2
        control::controller::ControllerUniquePtr controller;
        BOOST_REQUIRE_NO_THROW(
            controller = control::controller::createProportionalController(2));
        BOOST_TEST(controller->control(0) == 0);
        BOOST_TEST(controller->control(1) == 2);
        BOOST_TEST(controller->control(2) == 4);

        // gain = 5
        controller->gain(5.0);
        BOOST_TEST(controller->control(0) == 0);
        BOOST_TEST(controller->control(1) == 5);
        BOOST_TEST(controller->control(2) == 10);

        // gain = -2.6
        controller->gain(-2.6);
        BOOST_TEST(controller->control(0) == 0);
        BOOST_TEST(controller->control(1) =  -2.6);
        BOOST_TEST(controller->control(2) == -5.2);
    }
}
