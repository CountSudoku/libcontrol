#include <boost/test/unit_test.hpp>

#include "controller/derivative_controller.hpp"

namespace
{

    BOOST_AUTO_TEST_CASE(test_DerivativeController)
    {
        control::controller::ControllerUniquePtr controller;
        BOOST_REQUIRE_NO_THROW(
            controller = control::controller::createDerivativeController(1));
        BOOST_TEST(controller->control(6) == 0);
        BOOST_TEST(controller->control(5) == -1);
        BOOST_TEST(controller->control(10) == 5);
        BOOST_TEST(controller->control(0) == -10);
        BOOST_TEST(controller->gain() ==  1);
        BOOST_REQUIRE_NO_THROW(
            controller->gain(2));
        BOOST_TEST(controller->gain() == 2);
        BOOST_TEST(controller->control(-2) == -4);
        BOOST_TEST(controller->control(1) == 6);
    }
}
