#include <boost/test/unit_test.hpp>

#include "controller/constant_controller.hpp"

namespace
{
    BOOST_AUTO_TEST_CASE(test_ConstantController)
    {
        control::controller::ControllerUniquePtr controller;
        BOOST_REQUIRE_NO_THROW(
            controller = control::controller::createConstantController());

        BOOST_TEST(controller->control(0) == 1);
        BOOST_TEST(controller->control(100) == 1);
        BOOST_TEST(controller->control(-5) == 1);

        BOOST_TEST(controller->gain() == 1);
        BOOST_REQUIRE_NO_THROW(
            controller->gain(5));
        BOOST_TEST(controller->gain() == 5);

        BOOST_TEST(controller->control(100) == 5);
        BOOST_TEST(controller->control(-1) == 5);
    }
}
