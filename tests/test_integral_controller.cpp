#include <boost/test/unit_test.hpp>

#include "controller/integral_controller.hpp"

namespace
{

    BOOST_AUTO_TEST_CASE(test_IntegralController)
    {
        control::controller::ControllerUniquePtr controller;
        BOOST_REQUIRE_NO_THROW(
            controller = control::controller::createIntegralController(1));

        BOOST_TEST(controller->control(0) == 0);
        BOOST_TEST(controller->control(5) == 5);
        BOOST_TEST(controller->control(3) == 8);
        BOOST_TEST(controller->control(0) == 8);
    }

    BOOST_AUTO_TEST_CASE(
        test_IntegralController_gain, *boost::unit_test::tolerance(1e-12))
    {
        // gain = 2
        control::controller::ControllerUniquePtr controller;
        BOOST_REQUIRE_NO_THROW(
            controller = control::controller::createIntegralController(2));
        BOOST_TEST(controller->control(0) == 0);
        BOOST_TEST(controller->control(1) == 2);
        BOOST_TEST(controller->control(2) == 6);

        // gain = 5
        BOOST_TEST(controller->gain() == 2);
        BOOST_REQUIRE_NO_THROW(
            controller->gain(5));
        BOOST_TEST(controller->gain() == 5);
        BOOST_TEST(controller->control(0) == 15);
        BOOST_TEST(controller->control(1) == 20);
        BOOST_TEST(controller->control(-2) == 10);

        // gain = -2.6
        BOOST_TEST(controller->gain() == 5);
        BOOST_REQUIRE_NO_THROW(
            controller->gain(-2.6));
        BOOST_TEST(controller->gain() == -2.6);
        BOOST_TEST(controller->control(0) == -5.2);
        BOOST_TEST(controller->control(1) == -7.8);
        BOOST_TEST(controller->control(-2) == -2.6);
    }
}
