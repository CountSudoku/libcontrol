#include <boost/test/unit_test.hpp>

#include "compound_controller/pid_controller.hpp"

namespace {

    BOOST_AUTO_TEST_CASE(
        test_pidController, *boost::unit_test::tolerance(1e-12))
    {
        control::controller::Gains gains{1.0, 1.0, 1.0};
        auto controller = control::controller::createPidController(gains);
        BOOST_TEST(controller->control(0.0) == 0.0); // 0.0 + 0.0 + 0.0
        BOOST_TEST(controller->control(4.0) == 12.0); // 4.0 + 4.0 + 4.0
        BOOST_TEST(controller->control(2.0) == 6.0); // 2.0 + 6.0 - 2.0
        BOOST_TEST(controller->control(-5.3) == -11.9); // -5.3 + 0.7 - 7.3
        BOOST_TEST(controller->control(-2.5) == -1.5); // -2.5 - 1.8 + 2.8

        gains = {2.0, 0.5, 3.0};
        controller = control::controller::createPidController(gains);
        BOOST_TEST(controller->gains() == gains);
        BOOST_TEST(controller->control(0.0) ==  0.0);
        BOOST_TEST(controller->control(1.0) == 5.5); // 2.0 + 0.5 + 3.0
        BOOST_TEST(controller->control(-2.0) == -13.5); // -4.0 - 0.5 - 9.0
        BOOST_TEST(controller->control(0.0) == 5.5); // 0.0 - 0.5 + 6.0
        BOOST_TEST(controller->control(11.3) == 61.65); // 22.6 + 5.15 + 33.9
    }

    BOOST_AUTO_TEST_CASE(
        test_pidController_sampleTime, *boost::unit_test::tolerance(1e-12))
    {
        control::controller::Gains gains{1.0, 2.0, 0.5};
        auto controller = control::controller::createPidController(
            gains, std::chrono::milliseconds(500));
        BOOST_TEST(controller->control(0.0) == 0.0); // 0.0 + 0.0 + 0.0
        BOOST_TEST(controller->control(4.0) == 12.0); // 4.0 + 4.0 + 4.0
        BOOST_TEST(controller->control(2.0) == 6.0); // 2.0 + 6.0 - 2.0
        BOOST_TEST(controller->control(-5.3) == -11.9); // -5.3 + 0.7 - 7.3
        BOOST_TEST(controller->control(-2.5) == -1.5); // -2.5 - 1.8 + 2.8
    }

}
