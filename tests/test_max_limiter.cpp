#include "boost/test/unit_test.hpp"

#include "limiter/max_limiter.hpp"

namespace {

    BOOST_AUTO_TEST_CASE(test_maxLimiter)
    {
        double maxValue = 5.0;
        auto limiter = control::limiter::createMaxLimiter(maxValue);
        BOOST_TEST(limiter->limit(1e-17) == 1e-17);
        BOOST_TEST(limiter->limit(1.2) == 1.2);
        BOOST_TEST(limiter->limit(4.9999) == 4.9999);
        BOOST_TEST(limiter->limit(maxValue) == maxValue);
        BOOST_TEST(limiter->limit(5.0001) == maxValue);
        BOOST_TEST(limiter->limit(10) == maxValue);
        BOOST_TEST(limiter->limit(1e16) == maxValue);

        maxValue = 123;
        limiter = control::limiter::createMaxLimiter(maxValue);
        BOOST_TEST(limiter->limit(1e-17) == 1e-17);
        BOOST_TEST(limiter->limit(1.0) == 1.0);
        BOOST_TEST(limiter->limit(122.9) == 122.9);
        BOOST_TEST(limiter->limit(maxValue - 1e-10) != maxValue);
        BOOST_TEST(limiter->limit(maxValue) == maxValue);
        BOOST_TEST(limiter->limit(124) == maxValue);
        BOOST_TEST(limiter->limit(1e17) == maxValue);
        BOOST_TEST(limiter->limit(1e16) == maxValue);

    }

}
