#include <boost/test/unit_test.hpp>

#include "limiter/interval_limiter.hpp"

namespace {

    BOOST_AUTO_TEST_CASE(test_intervalLimiter)
    {
        double minValue = 13.0;
        double maxValue = 123;
        auto limiter = control::limiter::createIntervalLimiter(
            minValue, maxValue);
        BOOST_TEST(limiter->limit(1e-17) == minValue);
        BOOST_TEST(limiter->limit(12.9999) == minValue);
        BOOST_TEST(limiter->limit(minValue) == minValue);
        BOOST_TEST(limiter->limit(minValue + 1e-10) != minValue);
        BOOST_TEST(limiter->limit(13.0001) == 13.0001);
        BOOST_TEST(limiter->limit(122.9999) == 122.9999);
        BOOST_TEST(limiter->limit(maxValue - 1e-10) != maxValue);
        BOOST_TEST(limiter->limit(maxValue) == maxValue);
        BOOST_TEST(limiter->limit(123.0001) == maxValue);
        BOOST_TEST(limiter->limit(124) == maxValue);
        BOOST_TEST(limiter->limit(1e17) == maxValue);
    }
}
