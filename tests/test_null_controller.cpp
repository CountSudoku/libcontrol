#include <boost/test/unit_test.hpp>

#include "controller/null_controller.hpp"

namespace
{
    BOOST_AUTO_TEST_CASE(test_NullController)
    {
        control::controller::ControllerUniquePtr controller;
        BOOST_REQUIRE_NO_THROW(
            controller = control::controller::createNullController());

        BOOST_TEST(controller->control(0) == 0);
        BOOST_TEST(controller->control(100) == 0);

        BOOST_TEST(controller->gain() == 0);
        BOOST_REQUIRE_NO_THROW(
            controller->gain(2));
        BOOST_TEST(controller->gain() == 0);
    }
}
