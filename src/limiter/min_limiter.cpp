#include "limiter/min_limiter.hpp"

namespace {

    class MinLimiterImpl final
        : public control::limiter::Limiter
    {
    private: // fields
        const double _minValue;

    public: // con-/destruction
        explicit MinLimiterImpl(double minValue)
            : _minValue(minValue)
        { }

    public: // methods

        /*
         * Implements Limiter
         */
        virtual double limit(double value) override
        {
            return std::max(value, _minValue);
        }
    };
}

control::limiter::LimiterUniquePtr control::limiter::createMinLimiter(
    double minValue)
{
    return control::limiter::LimiterUniquePtr(new MinLimiterImpl(minValue));
}
