#include "limiter/interval_limiter.hpp"

namespace {

    class IntervalLimiterImpl final
        : public control::limiter::Limiter
    {
    private: // fields
        const double _minValue;
        const double _maxValue;

    public: // con-/destruction
        IntervalLimiterImpl(double minValue, double maxValue)
            : _minValue(minValue)
            , _maxValue(maxValue)
        { }

    public: // methods

        /*
         * Implements Limiter
         */
        virtual double limit(double value) override
        {
            return std::min(std::max(value, _minValue), _maxValue);
        }
    };
}

control::limiter::LimiterUniquePtr control::limiter::createIntervalLimiter(
    double minValue, double maxValue)
{
    return control::limiter::LimiterUniquePtr(
        new IntervalLimiterImpl(minValue, maxValue));
}
