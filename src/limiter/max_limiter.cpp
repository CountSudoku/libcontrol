#include "limiter/max_limiter.hpp"

namespace {

    class MaxLimiterImpl final
        : public control::limiter::Limiter
    {
    private: // fields
        const double _maxValue;

    public: // con-/destruction
        explicit MaxLimiterImpl(double maxValue)
            : _maxValue(maxValue)
        { }

    public: // methods

        /*
         * Implements Limiter
         */
        virtual double limit(double value) override
        {
            return std::min(value, _maxValue);
        }
    };
}

control::limiter::LimiterUniquePtr control::limiter::createMaxLimiter(
    double maxValue)
{
    return control::limiter::LimiterUniquePtr(new MaxLimiterImpl(maxValue));
}
