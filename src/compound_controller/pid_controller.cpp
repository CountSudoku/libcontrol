#include "compound_controller/pid_controller.hpp"

#include <stdexcept>

#include "controller/proportional_controller.hpp"
#include "controller/integral_controller.hpp"
#include "controller/derivative_controller.hpp"

namespace {

    class PIDController final
        : public control::controller::CompoundController
    {
    private: // fields

        control::controller::ControllerUniquePtr _proportionalController;
        control::controller::ControllerUniquePtr _integralController;
        control::controller::ControllerUniquePtr _derivativeController;

    public: // con-/destruction

        PIDController()
            : _proportionalController(
                control::controller::createProportionalController(1))
            , _integralController(
                control::controller::createIntegralController(1))
            , _derivativeController(
                control::controller::createDerivativeController(1))
        { }

        explicit PIDController(const std::chrono::nanoseconds& sampleTime)
            : _proportionalController(
                control::controller::createProportionalController(1))
            , _integralController(
                control::controller::createIntegralController(1, sampleTime))
            , _derivativeController(
                control::controller::createDerivativeController(1, sampleTime))
        { }

        PIDController(
            double proportionalGain,
            double integralGain,
            double derivativeGain,
            const std::chrono::nanoseconds& sampleTime)
            : _proportionalController(
                control::controller::createProportionalController(
                    proportionalGain))
            , _integralController(
                control::controller::createIntegralController(
                    integralGain, sampleTime))
            , _derivativeController(
                control::controller::createDerivativeController(
                    derivativeGain, sampleTime))
        { }

        PIDController(
            control::controller::ControllerUniquePtr&& proportionalController,
            control::controller::ControllerUniquePtr&& integralController,
            control::controller::ControllerUniquePtr&& derivativeController)
            : _proportionalController(std::move(proportionalController))
            , _integralController(std::move(integralController))
            , _derivativeController(std::move(derivativeController))
        { }

    public: // methods

        virtual double control(double error) override
        {
            return _proportionalController->control(error) +
                _integralController->control(error) +
                _derivativeController->control(error);
        }

        virtual control::controller::Gains gains() const override
        {
            return control::controller::Gains(
                {
                    _proportionalController->gain(),
                    _integralController->gain(),
                    _derivativeController->gain()
                });
        }

        virtual void gains(const control::controller::Gains& values) override
        {
            if (values.size() != 3) {
                throw std::out_of_range("Wrong number of Gains");
            }

            _proportionalController->gain(values[0]);
            _integralController->gain(values[1]);
            _derivativeController->gain(values[2]);
        }

    };
}

control::controller::CompoundControllerUniquePtr
    control::controller::createPidController(
        double proportionalGain,
        double integralGain,
        double differencialGain,
        const std::chrono::nanoseconds& sampleTime)
{
    return control::controller::CompoundControllerUniquePtr(
        new PIDController(
            proportionalGain, integralGain, differencialGain, sampleTime));
}

control::controller::CompoundControllerUniquePtr
    control::controller::createPidController(
        const control::controller::Gains& gains,
        const std::chrono::nanoseconds& sampleTime)
{
    auto result = control::controller::CompoundControllerUniquePtr(
        new PIDController(sampleTime));
    result->gains(gains);
    return std::move(result);
}


control::controller::CompoundControllerUniquePtr
    control::controller::createTimedPidController(
        double proportionalGain,
        double integralGain,
        double derivativeGain)
{
    return control::controller::CompoundControllerUniquePtr(
        new PIDController(
            std::move(createProportionalController(proportionalGain)),
            std::move(createTimedIntegralController(integralGain)),
            std::move(createTimedDerivativeController(derivativeGain))));

}
