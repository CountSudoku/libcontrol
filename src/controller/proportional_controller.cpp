#include "controller/proportional_controller.hpp"

#include <mutex>

namespace {

    class ProportionalControllerImpl final
        : public control::controller::Controller
    {
    private: // fields

        double _gain;
        mutable std::mutex _mutex;

    public: // con-/destruction

        ProportionalControllerImpl()
            : _gain(1)
        { }

        ProportionalControllerImpl(double gain)
            : _gain(gain)
        { }

    public: // methods

        /*
         * Implements Controller
         */
        virtual double control(double error) override
        {
            double tmp;
            {
                std::lock_guard<std::mutex> lock(_mutex);
                tmp = _gain;
            }
            return tmp * error;
        }

        /*
         * Implements Controller
         */
        virtual double gain() const override
        {
            std::lock_guard<std::mutex> lock(_mutex);
            return _gain;
        }

        /*
         * Implements Controller
         */
        virtual void gain(double value) override
        {
            std::lock_guard<std::mutex> lock(_mutex);
            _gain = value;
        }

    };
}

control::controller::ControllerUniquePtr
    control::controller::createProportionalController(double gain)
{
    return control::controller::ControllerUniquePtr(
        new ProportionalControllerImpl(gain));
}
