#include "controller/constant_controller.hpp"

#include <mutex>

namespace {

    class ConstantControllerImpl final
        : public control::controller::Controller
    {
    private: // fields

        double _gain;
        mutable std::mutex _mutex;

    public: // con-/destruction

        ConstantControllerImpl()
            : _gain(1)
        { }

        ConstantControllerImpl(double gain)
            : _gain(gain)
        { }

    public: // methods

        /*
         * Implements Controller
         */
        virtual double control(double error) override
        {
            (void) error;
            std::lock_guard<std::mutex> lock(_mutex);
            return _gain;
        }

        /*
         * Implements Controller
         */
        virtual double gain() const override
        {
            std::lock_guard<std::mutex> lock(_mutex);
            return _gain;
        }

        /*
         * Implements Controller
         */
        virtual void gain(double value) override
        {
            std::lock_guard<std::mutex> lock(_mutex);
            _gain = value;
        }

    };
}

control::controller::ControllerUniquePtr
    control::controller::createConstantController(double gain)
{
    return control::controller::ControllerUniquePtr(
        new ConstantControllerImpl(gain));
}
