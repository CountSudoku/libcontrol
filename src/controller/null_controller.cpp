#include "controller/null_controller.hpp"

namespace {

    class NullControllerImpl final
        : public control::controller::Controller
    {
    public: // con-destruction

        NullControllerImpl() = default;

    public: // methods

        /*
         * Implements Controller
         */
        virtual double control(double error) override
        {
            (void) error;
            return 0.0;
        }

        /*
         * Implements Controller
         */
        virtual double gain() const override
        {
            return 0.0;
        }

        /*
         * Implements Controller
         */
        virtual void gain(double value) override
        {
            (void) value;
        }

    };
}

control::controller::ControllerUniquePtr
    control::controller::createNullController()
{
    return control::controller::ControllerUniquePtr(
        new NullControllerImpl());
}
