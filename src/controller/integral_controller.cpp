#include "controller/integral_controller.hpp"

#include <mutex>

#include "interval_timer.hpp"
#include "interval_timer/constant_interval_timer.hpp"
#include "interval_timer/interval_timer_impl.hpp"

namespace {

    class IntegralControllerImpl final
        : public control::controller::Controller
    {
    private: // fields

        double _errorSum;
        double _gain;
        control::IntervalTimerUniquePtr _intervalTimer;
        mutable std::mutex _mutex;

    public: // con-/destruction

        IntegralControllerImpl()
            : _errorSum(0)
            , _gain(1)
            , _intervalTimer(
                control::createConstantIntervalTimer(std::chrono::seconds{1}))
        { }

        IntegralControllerImpl(
            double gain,
            control::IntervalTimerUniquePtr&& intervalTimer)
            : _errorSum(0)
            , _gain(gain)
            , _intervalTimer(std::move(intervalTimer))
        { }

    public: // methods

        /**
         * @brief resets the internal state
         */
        void reset()
        {
            _errorSum = 0;
        }

        /*
         * Implements Controller
         */
        virtual double control(double error) override
        {
            std::lock_guard<std::mutex> lock(_mutex);
            _errorSum += error;
            control::DoubleSeconds seconds = _intervalTimer->measure();
            return _gain * _errorSum * seconds.count();
        }

        /*
         * Implements Controller
         */
        virtual double gain() const override
        {
            std::lock_guard<std::mutex> lock(_mutex);
            return _gain;
        }

        virtual void gain(double value) override
        {
            std::lock_guard<std::mutex> lock(_mutex);
            _gain = value;
        }

    };

}

control::controller::ControllerUniquePtr
    control::controller::createIntegralController(
        double gain,
        const std::chrono::nanoseconds& sampleTime)
{
    return control::controller::ControllerUniquePtr(
        new IntegralControllerImpl(
            gain,
            std::move(control::createConstantIntervalTimer(sampleTime))));
}

control::controller::ControllerUniquePtr
    control::controller::createTimedIntegralController(double gain)
{
    return control::controller::ControllerUniquePtr(
        new IntegralControllerImpl(
            gain,
            std::move(control::createIntervalTimer())));
}
