#include "controller/derivative_controller.hpp"

#include <mutex>

#include "interval_timer.hpp"
#include "interval_timer/constant_interval_timer.hpp"
#include "interval_timer/interval_timer_impl.hpp"

namespace {

    class DerivativeControllerImpl final
        : public control::controller::Controller
    {
    private: // fields

        bool _initalized;
        double _lastError;
        double _gain;
        control::IntervalTimerUniquePtr _intervalTimer;
        mutable std::mutex _mutex;

    public: // con-/destruction

        DerivativeControllerImpl()
            : _initalized(false)
            , _lastError(0)
            , _gain(1)
            , _intervalTimer(
                control::createConstantIntervalTimer(std::chrono::seconds{1}))
        { }

        DerivativeControllerImpl(
            double gain,
            control::IntervalTimerUniquePtr&& intervalTimer)
            : _initalized(false)
            , _lastError(0)
            , _gain(gain)
            , _intervalTimer(std::move(intervalTimer))
        { }

    public: // methods

        /*
         * Implements Controller
         */
        virtual double control(double error) override
        {
            std::lock_guard<std::mutex> lock(_mutex);
            double result;
            if (not _initalized) {
                _initalized = true;
                _lastError = error;
                result = 0;
            } else {
                control::DoubleSeconds seconds = _intervalTimer->measure();
                result = _gain * (error - _lastError) / seconds.count();
                _lastError = error;
            }
            return result;
        }

        /*
         * Implements Controller
         */
        virtual double gain() const override
        {
            std::lock_guard<std::mutex> lock(_mutex);
            return _gain;
        }

        /*
         * Implements Controller
         */
        virtual void gain(double value) override
        {
            std::lock_guard<std::mutex> lock(_mutex);
            _gain = value;
        }

    };
}

control::controller::ControllerUniquePtr
    control::controller::createDerivativeController(
        double gain,
        const std::chrono::nanoseconds& sampleTime)
{
    return control::controller::ControllerUniquePtr(
        new DerivativeControllerImpl(
            gain,
            std::move(control::createConstantIntervalTimer(sampleTime))));
}

control::controller::ControllerUniquePtr
    control::controller::createTimedDerivativeController(double gain)
{
    return control::controller::ControllerUniquePtr(
        new DerivativeControllerImpl(
            gain,
            std::move(control::createIntervalTimer())));
}
