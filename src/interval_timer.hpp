#ifndef LIB_CONTROL_INTERVAL_TIMER
#define LIB_CONTROL_INTERVAL_TIMER

#include <chrono>
#include <memory>

namespace control
{

    using DoubleSeconds = std::chrono::duration<double, std::ratio<1> >;

    /**
     * @brief Interface for time interval measuring classes
     */
    class IntervalTimer
    {
    public: // con-/destruction

        virtual ~IntervalTimer();

    public: // methods

        /**
         * @brief measure time since last call
         */
        virtual DoubleSeconds measure() = 0;

    };

    using IntervalTimerUniquePtr = std::unique_ptr<IntervalTimer>;
    using IntervalTimerSharedPtr = std::shared_ptr<IntervalTimer>;
}

#endif
