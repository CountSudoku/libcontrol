#ifndef LIB_CONTROL_INTERVAL_TIMER_CONSTANT_TIMER
#define LIB_CONTROL_INTERVAL_TIMER_CONSTANT_TIMER

#include "interval_timer.hpp"

namespace  control
{
    /**
     * @brief create a interval timer that returns constant measures
     */
    control::IntervalTimerUniquePtr createConstantIntervalTimer(
        const std::chrono::nanoseconds& ns);
}

#endif
