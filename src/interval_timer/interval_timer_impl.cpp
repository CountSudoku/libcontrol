#include "interval_timer_impl.hpp"

#include <chrono>
#include <mutex>

namespace {

    class IntervalTimerImpl
        : public control::IntervalTimer
    {
    private: // nested types

        using Clock = std::chrono::steady_clock;
        using TimePoint = std::chrono::time_point<Clock>;

    private: // fields

        TimePoint _lastTimePoint;
        mutable std::mutex _mutex;

    public: // con-/destruction

        IntervalTimerImpl()
            : _lastTimePoint(TimePoint::min())
        { }

    public: // methods

        /*
         * Implements IntervalTimer
         */
        virtual control::DoubleSeconds measure() override
        {
            auto now = Clock::now();
            std::lock_guard<std::mutex> lock(_mutex);
            if (_lastTimePoint == TimePoint::min()) {
                _lastTimePoint = now;
            }
            auto result = now - _lastTimePoint;
            _lastTimePoint = now;
            return result;
        }
    };
}

control::IntervalTimerUniquePtr control::createIntervalTimer()
{
    return control::IntervalTimerUniquePtr(
        new IntervalTimerImpl());
}
