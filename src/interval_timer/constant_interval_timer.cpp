#include "constant_interval_timer.hpp"

namespace {

    class ConstantIntervalTimerImpl final
        : public control::IntervalTimer
    {

    private: // fields

        control::DoubleSeconds _timeDuration;

    public: // con-/destruction

        explicit ConstantIntervalTimerImpl(const std::chrono::nanoseconds& ns)
            : _timeDuration(ns)
        { }

    public: // methods

        /*
         * Implements IntervalTimer
         */
        virtual control::DoubleSeconds measure() override
        {
            return _timeDuration;
        }

    };
}

control::IntervalTimerUniquePtr control::createConstantIntervalTimer(
    const std::chrono::nanoseconds& ns)
{
    return control::IntervalTimerUniquePtr(
        new ConstantIntervalTimerImpl(ns));
}
