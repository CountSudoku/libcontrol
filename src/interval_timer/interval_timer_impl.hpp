#ifndef LIB_CONTROL_INTERVAL_TIMER_INTERVAL_TIMER_IMPL
#define LIB_CONTROL_INTERVAL_TIMER_INTERVAL_TIMER_IMPL

#include "interval_timer.hpp"

namespace control
{

    /**
     * @brief creates a interval timer that measures wall time
     */
    control::IntervalTimerUniquePtr createIntervalTimer();
}

#endif
