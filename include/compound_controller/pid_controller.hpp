#ifndef LIB_CONTROL_IMPL_PID_CONTROLLER
#define LIB_CONTROL_IMPL_PID_CONTROLLER

#include "compound_controller.hpp"

#include <chrono>

namespace control
{
    namespace controller
    {

        /**
         * @brief creates a PID controller
         *
         * @param proportionalGain: the gain value for the proportional part of
         * the controller.
         * @param integralGain: the gain value for the integral part of the
         * controller.
         * @param derivativeGain: the gain value for the derivative part of the
         * controller.
         * @param sampleTime: the duration between calls to control
         */
        CompoundControllerUniquePtr createPidController(
            double proportionalGain,
            double integralGain,
            double derivativeGain,
            const std::chrono::nanoseconds& sampleTime=std::chrono::seconds{1});

        /**
         * @brief creates a PID controller
         *
         * @param gains: a vector of gains for every sub controller
         * @param sampleTime: the duration between calls to control
         */
        CompoundControllerUniquePtr createPidController(
            const Gains& gains,
            const std::chrono::nanoseconds& sampleTime=std::chrono::seconds{1});

        /**
         * @brief creates a PID controller, which takes measured time interval
         * between consecutive calls to control into account.
         *
         * @param proportionalGain: the gain value for the proportional part of
         * the controller.
         * @param integralGain: the gain value for the integral part of the
         * controller.
         * @param derivativeGain: the gain value for the derivative part of the
         * controller.
         *
         * @remarks
         *  The resulting controller will measure the time interval between
         *  calls to control. This should only be used, if no stable control
         *  loop can be established. If you have a (nearly) equidistant timed
         *  control loop use @ref createPidController() instead.
         */
        CompoundControllerUniquePtr createTimedPidController(
            double proportionalGain,
            double integralGain,
            double derivativeGain);

    }
}
#endif
