#ifndef LIB_CONTROL_LIMITER
#define LIB_CONTROL_LIMITER

#include <memory>

namespace control
{
    namespace limiter
    {
        /**
         * @brief Interface to limit a value in various directions
         */
        class Limiter
        {
        public: // con-/destruction

            virtual ~Limiter();

        public: // methods

            virtual double limit(double value) = 0;

        };

        using LimiterUniquePtr = std::unique_ptr<Limiter>;
        using LimiterSharedPtr = std::shared_ptr<Limiter>;
    }
}
#endif
