#ifndef LIB_CONTROL_CONTROLLER
#define LIB_CONTROL_CONTROLLER

#include <vector>
#include <memory>

namespace control
{

    namespace controller
    {
        /**
         * @brief basic controller interface
         */
        class Controller
        {
        public: // con-/destruction

            virtual ~Controller();

        public: // methods

            /**
             * @brief respond to given error
             *
             * @param error: The difference between the actual value and the
             * set value.
             *
             * @returns the controller response
             */
            virtual double control(double error) = 0;

            /**
             * @brief getter for the used gain
             */
            virtual double gain() const = 0;

            /**
             * @brief setter for the used gain
             */
            virtual void gain(double value) = 0;

        };

        using ControllerUniquePtr = std::unique_ptr<Controller>;
        using ControllerSharedPtr = std::shared_ptr<Controller>;

    }
}

#endif
