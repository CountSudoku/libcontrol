#ifndef LIB_CONTROL_IMPL_NULL_CONTROLLER
#define LIB_CONTROL_IMPL_NULL_CONTROLLER

#include "controller.hpp"

namespace control
{
    namespace controller
    {

        /**
         * @brief creates a trivial controller
         *
         * This controller will always respond with 0.
         *
         * You can set the gain, but the value will be ignored.
         *
         * @remarks
         *  The resulting controller is thread-safe
         */
        ControllerUniquePtr createNullController();
    }
}
#endif
