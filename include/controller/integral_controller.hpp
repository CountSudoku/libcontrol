#ifndef LIB_CONTROL_IMPL_INTEGRAL_CONTROLLER
#define LIB_CONTROL_IMPL_INTEGRAL_CONTROLLER

#include "controller.hpp"

#include <chrono>

namespace control
{
    namespace controller
    {

    /**
     * @brief create an integral controller for equidistant control loop time
     * interval
     *
     * @param gain: factor to scale the response
     * @param sampleTime: The sample time between two consecutive calls to
     * control
     *
     * This controller has a internal state. If you want to reset this state,
     * you have to replace it with a new instance. Since this is a lightweight
     * object, this should be no problem.
     *
     * @remarks
     *  the resulting controller is thread-safe
     */
    ControllerUniquePtr createIntegralController(
        double gain=1,
        const std::chrono::nanoseconds& sampleTime = std::chrono::seconds{1});

    /**
     * @brief creates a integral controller with measured timings between
     * control calls
     *
     * @param gain: factor to scale the response
     *
     * @remarks
     *  The resulting controller will measure the time interval between
     *  consecutive calls to control. This should only be used, if no stable
     *  control loop can be established. If you have a (nearly) equidistant
     *  timed control loop use @ref createIntegralController() instead.
     */
    ControllerUniquePtr createTimedIntegralController(double gain=1);

    }
}

#endif
