#ifndef LIB_CONTROL_IMPL_PROPORTIONAL_CONTROLLER
#define LIB_CONTROL_IMPL_PROPORTIONAL_CONTROLLER

#include "controller.hpp"

namespace  control
{
    namespace controller
    {

        /**
         * @brief creates a controller with proportional response
         *
         * @param gain: the factor for the proportional response
         *
         * @remarks the resulting controller is thread-safe.
         */
        ControllerUniquePtr createProportionalController(double gain=1);

    }
}
#endif
