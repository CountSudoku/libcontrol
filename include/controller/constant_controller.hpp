#ifndef LIB_CONTROL_IMPL_CONSTANT_CONTROLLER
#define LIB_CONTROL_IMPL_CONSTANT_CONTROLLER

#include "controller.hpp"

namespace control
{

    namespace controller
    {
        /**
         * @brief Creates controller, which always response with the same value
         *
         * @param gain: controls the amplitude of the constant response.
         * Without this parameter the response will always be 1.
         *
         * @remarks
         *  The created controller is thread safe.
         */
        ControllerUniquePtr createConstantController(double gain=1);

    }
}
#endif
