#ifndef LIB_CONTROL_COMPOUND_CONTROLLER
#define LIB_CONTROL_COMPOUND_CONTROLLER

#include <vector>
#include <memory>

namespace control
{
    namespace controller
    {

        using Gains = std::vector<double>;

        /**
         * @brief interface for combined controllers
         */
        class CompoundController
        {
        public: // con-/destruction

            virtual ~CompoundController();

        public: // methods

            /**
             * @brief respond to given error
             *
             * @param error: The difference between the actual value and the
             * set value.
             *
             * @returns the controller response
             */
            virtual double control(double error) = 0;

            /**
             * @brief getter for the used gains
             */
            virtual Gains gains() const = 0;

            /**
             * @brief setter for the used gains
             */
            virtual void gains(const Gains& values) = 0;
        };

        using CompoundControllerUniquePtr =
            std::unique_ptr<CompoundController>;
        using CompoundControllerSharedPtr =
            std::shared_ptr<CompoundController>;

    }
}
#endif
