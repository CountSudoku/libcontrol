#ifndef LIB_CONTROL_LIMITER_MAX_LIMITER
#define LIB_CONTROL_LIMITER_MAX_LIMITER

#include "limiter.hpp"

namespace control
{
    namespace limiter
    {
        /**
         * @brief creates a limiter that limits a value upwards.
         *
         * @param maxValue: the top limit
         */
        LimiterUniquePtr createMaxLimiter(double maxValue);
    }
}
#endif
