#ifndef LIB_CONTROL_LIMITER_MIN_LIMITER
#define LIB_CONTROL_LIMITER_MIN_LIMITER

#include "limiter.hpp"

namespace control
{
    namespace limiter
    {
        /**
         * @brief creates a limiter that limits a value downwards
         *
         * @param minValue: the bottom limit
         */
        LimiterUniquePtr createMinLimiter(double minValue);
    }
}
#endif
