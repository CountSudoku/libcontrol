#ifndef LIB_CONTROL_LIMITER_INTERVAL_LIMITER
#define LIB_CONTROL_LIMITER_INTERVAL_LIMITER

#include "limiter.hpp"

namespace control
{
    namespace limiter
    {
        /**
         * @brief creates a limiter that limits a value on both sides
         *
         * @param minValue: the bottom limit
         * @param maxValue: the top limit
         */
        LimiterUniquePtr createIntervalLimiter(
            double minValue, double maxValue);
    }
}
#endif
